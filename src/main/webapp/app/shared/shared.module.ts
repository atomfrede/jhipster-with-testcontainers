import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { JhtcSharedLibsModule, JhtcSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [JhtcSharedLibsModule, JhtcSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [JhtcSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhtcSharedModule {
  static forRoot() {
    return {
      ngModule: JhtcSharedModule
    };
  }
}
